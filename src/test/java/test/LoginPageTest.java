package test;

import constant.UserData;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;
import page.HomePage;
import page.LoginPage;

public class LoginPageTest {
    private WebDriver driver;

    @BeforeClass
    public void setupDriver() {
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--remote-allow-origins=*");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(option);
        driver.manage().window().maximize();
    }

    @Test
    public void checkPlaceHolder() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.verifyPlaceHolder();
    }

    @Test
    public void testSignInWithValidData() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.signIn(UserData.VALID_USERNAME, UserData.VALID_PASSWORD);
        HomePage homePage = new HomePage(driver);
        homePage.verifyIsInHomePage();
    }

    @AfterClass
    public void closeBrowser() {
        driver.quit();
    }
}
