package page;

import constant.LoginPageLocator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private WebDriver driver;

    @FindBy(xpath = LoginPageLocator.USERNAME)
    private WebElement username;

    @FindBy(xpath = LoginPageLocator.PASSWORD)
    private WebElement password;

    @FindBy(xpath = LoginPageLocator.LOGIN_BUTTON)
    private WebElement loginButton;

    @FindBy(xpath = LoginPageLocator.SIGN_IN_ERROR_TEXT)
    private WebElement signInErrorText;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        driver.get(LoginPageLocator.URL);
    }

    public void setUserEmail(String usersEmail) {
        username.clear();
        username.sendKeys(usersEmail);
    }

    public void setUserPassword(String usersPassword) {
        password.clear();
        password.sendKeys(usersPassword);
    }

    public void signIn(String usersEmail, String password) {
        setUserEmail(usersEmail);
        setUserPassword(password);
        clickLogin();
    }

    public void clickLogin() {
        loginButton.click();
    }

    public void verifyPlaceHolder() {
        assert username.getAttribute("placeholder").equals("Username");
        assert password.getAttribute("placeholder").equals("Password");
    }

    public void errorMessageIsVisible() {
        assert signInErrorText.isDisplayed();
    }
}
