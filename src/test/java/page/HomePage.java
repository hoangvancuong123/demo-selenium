package page;

import constant.HomePageLocator;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    private WebDriver driver;

    @FindBy(xpath = HomePageLocator.FORM_TITLE)
    private WebElement formTitle;

    @FindBy(xpath = HomePageLocator.LOGIN_FORM)
    private WebElement formSubmit;

    @FindBy(xpath = HomePageLocator.READ_MISSION_CHECKBOX)
    private WebElement checkBoxReadMission;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void verifyIsInHomePage() {
        formTitle.isDisplayed();
        formSubmit.isDisplayed();
    }

    public void clickReadMission() {
        checkBoxReadMission.click();
    }

    public void verifyCheckBoxReadMissionIsChecked() {
        String script = "return document.getElementById('chk_hospotal_readmission').checked";
        JavascriptExecutor js = (JavascriptExecutor) driver;
        var isChecked = js.executeScript(script, "");
        assert isChecked.equals(true);
    }
}
