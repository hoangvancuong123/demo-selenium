package constant;

public class HomePageLocator {
    public static final String FORM_TITLE = "//h2[text()='Make Appointment']";
    public static final String LOGIN_FORM = "//form[@action='https://katalon-demo-cura.herokuapp.com/appointment.php#summary']";
    public static final String READ_MISSION_CHECKBOX = "//input[@id='chk_hospotal_readmission']";
}
