package constant;

public class LoginPageLocator {
    public static final String URL = "https://katalon-demo-cura.herokuapp.com/profile.php#login";
    public static final String USERNAME = "//input[@id='txt-username']";
    public static final String PASSWORD = "//input[@id='txt-password']";
    public static final String LOGIN_BUTTON = "//button[@id='btn-login']";
    public static final String SIGN_IN_ERROR_TEXT = "//p[@class='lead text-danger']";
}
