package main.logic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class LogicTest {
    public static void main(String[] args) throws Exception {
        String startDateInput = "20000331";
        String endDateInput = "20020227";
        System.out.println(calculateDuration(convertToDate(startDateInput, ""),convertToDate(endDateInput, "")));
        System.out.println(getCloseDate(startDateInput, 23));
    }

    public static Date convertToDate(String dateStr, String dateFormat) throws ParseException {
        String format = dateFormat.equals("") ? "dd/MM/yyyy" : dateFormat;
        SimpleDateFormat originFormatter = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat targetFormatter = new SimpleDateFormat(format);
        return targetFormatter.parse(targetFormatter.format(originFormatter.parse(dateStr)));
    }

    public static int calculateDuration(Date startDate, Date endDate) throws Exception {
        if (endDate.compareTo(startDate) < 0) {
            throw new Exception("Invalid input: start date is ahead of end date");
        }
        long timeDurationMil = endDate.getTime() - startDate.getTime();
        return (int) (timeDurationMil / (1000*60*60*24)) + 1;
    }

    public static String getCloseDate(String startDate, int monthDuration) {
        ArrayList<Integer> month31Day = new ArrayList<>(Arrays.asList(1,3,5,7,8,10,12));
        Calendar calendar = Calendar.getInstance();
        GregorianCalendar gregorianCalendar = (GregorianCalendar) calendar;

        int targetDay, targetYear, targetMonth;
        int year = Integer.parseInt(startDate.substring(0, 4));
        int month = Integer.parseInt(startDate.substring(4, 6));
        int day = Integer.parseInt(startDate.substring(6, 8));

        int yearDuration = monthDuration/12 + (month + (monthDuration%12))/12;
        targetYear = year + yearDuration;
        targetMonth = (month + (monthDuration%12))%12;

        boolean targetYearIsLeapYear = gregorianCalendar.isLeapYear(targetYear);
        boolean startYearIsLeapYear = gregorianCalendar.isLeapYear(year);

        if (day < 28) {
            targetDay = day;
        } else if (day == 28) {
            if (month == 2) {
                if (startYearIsLeapYear) {
                    targetDay = 28;
                } else {
                    if (targetYearIsLeapYear && targetMonth == 2) {
                        targetDay = 29;
                    } else if (!targetYearIsLeapYear && targetMonth == 2) {
                        targetDay = 28;
                    } else {
                        targetDay = month31Day.contains(targetMonth) ? 31 : 30;
                    }
                }
            } else {
                targetDay = 28;
            }
        } else if (day == 29) {
            if (month == 2) {
                if (targetYearIsLeapYear) {
                    if (targetMonth == 2) {
                        targetDay = 29;
                    } else {
                        targetDay = month31Day.contains(targetMonth) ? 31 : 30;
                    }
                } else {
                    if (targetMonth == 2) {
                        targetDay = 28;
                    } else {
                        targetDay = month31Day.contains(targetMonth) ? 31 : 30;
                    }
                }
            } else {
                if (targetYearIsLeapYear) {
                    targetDay = 29;
                } else {
                    if (targetMonth == 2) {
                        targetDay = 28;
                    } else {
                        targetDay = 29;
                    }
                }
            }
        } else if (day == 30) {
            if (month31Day.contains(month)) {
                if (targetYearIsLeapYear) {
                    if (targetMonth == 2) {
                        targetDay = 29;
                    } else {
                        targetDay = 30;
                    }
                } else {
                    if (targetMonth == 2) {
                        targetDay = 28;
                    } else {
                        targetDay = 30;
                    }
                }
            } else {
                if (targetYearIsLeapYear) {
                    if (targetMonth == 2) {
                        targetDay = 29;
                    } else {
                        targetDay = 31;
                    }
                } else {
                    if (targetMonth == 2) {
                        targetDay = 28;
                    } else {
                        targetDay = 31;
                    }
                }
            }
        } else {
            if (targetYearIsLeapYear) {
                if (targetMonth == 2) {
                    targetDay = 29;
                } else {
                    targetDay = month31Day.contains(targetMonth) ? 31 : 30;
                }
            } else {
                if (targetMonth == 2) {
                    targetDay = 28;
                } else {
                    targetDay = month31Day.contains(targetMonth) ? 31 : 30;
                }
            }
        }
        return targetYear + String.format("%02d", targetMonth) +  String.format("%02d", targetDay);
    }
}
